# Propel dotfiles

For use with your project's dev book.

This is based on https://github.com/geerlingguy/dotfiles, but made even more minimalist.

## License

MIT / BSD
